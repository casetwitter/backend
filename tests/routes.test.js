const request = require('supertest');

const app = require('../config/express');
const http = require('http');

describe('Validar as rotas do backend', () => {
    const port = 3002;
    let server;

    beforeAll(done => {
        server = http.createServer(app);
        server.listen(3002, done)
    });

    afterAll(done => {
        server.close(done);
    });

    describe('Teste dos Endpoints da API', () => {

        /*it('Deve retornar pelo menos 100 tweets da #docker e salvar no banco de dados', async () => {
            const res = await request(server).get('/api/tweet').query({ hashtag: '#docker' });
            expect(res.statusCode).toEqual(200);
            expect(res.body.length).toBeLessThanOrEqual(100);
        });

        it('Deve retornar os cinco usuários com maior numero de seguidores', async () => {
            const res = await request(server).get('/api/tweet/users')
            expect(res.statusCode).toEqual(200);
            expect(res.body).toHaveLength(13);
            expect(res.body[5].ranking.length).toBeGreaterThan(0);
        });*/

        it('Deve retornar as hashtags já cadastradas no banco de dados', async () => {
            const res = await request(server).get('/api/tweet/hashtag/stored/');
            expect(res.statusCode).toEqual(200);
        });

        it('Deve retornar num erro para um rota inexistente', async () => {
            const res = await request(server).get('/nao/existe');
            expect(res.statusCode).toEqual(404);
        });
    });
});