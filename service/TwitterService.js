const Twit = require('twit');
const env = require('../config/env');

var T = new Twit({
    consumer_key: env.CONSUMER_KEY,
    consumer_secret: env.CONSUMER_SECRET,
    access_token: env.ACCESS_TOKEN,
    access_token_secret: env.ACCESS_TOKEN_SECRET,
    timeout_ms: 60 * 1000
});

module.exports = {
    findTweetsByHashtag: async (hashtag) => {
        return new Promise((resolve, reject) => {
            T.get('search/tweets', {
                q: `${hashtag}`,
                count: 100,
                tweet_mode: "extended"
            }, (err, data, response) => {
                let listTweets = data.statuses.map((item) => {
                    let tweet = {};
                    tweet['text'] = (item.retweeted_status) ? item.retweeted_status.full_text : item.full_text;
                    tweet['date'] = item.created_at;
                    tweet['name'] = item.user.name;
                    tweet['screen_name'] = item.user.screen_name;
                    tweet['img'] = item.user.profile_image_url;
                    tweet['followers'] = item.user.followers_count;
                    tweet['hashtag'] = hashtag;
                    return tweet;
                });
                resolve(listTweets);
            });
        });
    }
}