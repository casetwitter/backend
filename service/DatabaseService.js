const nano = require('nano');

let DB_URL = process.env.DB_URL || '127.0.0.1';
const DB_PORT = process.env.DB_PORT || 5984;
const DB_USER = process.env.DB_USER || 'admin';
const DB_PASSWORD = process.env.DB_USER || 'admin';
let DB_NAME = 'tweets';

if (process.env.NODE_ENV === 'test') {
    DB_URL = process.env.DB_URL_TEST || '127.0.0.1';
    DB_NAME = 'tweets_test';
}

const conn = nano(`http://${DB_USER}:${DB_PASSWORD}@${DB_URL}:${DB_PORT}`);

const database = conn.db.use(DB_NAME);

module.exports = {
    getDatabase: () => {
        return database;
    },
    insertTweets: (tweet) => {
        return new Promise((resolve, reject) => {
            database.insert(tweet).then((_) => resolve()).catch((err) => reject(err))
        });
    }
}