#!/usr/bin/env bash
set -x
set -e

{
    ENV_FILE=.env

    touch $ENV_FILE
    chmod 600 $ENV_FILE

    echo "CONSUMER_KEY=${CONSUMER_KEY}"                >> $ENV_FILE
    echo "CONSUMER_SECRET=${CONSUMER_SECRET}"          >> $ENV_FILE
    echo "ACCESS_TOKEN=${ACCESS_TOKEN}"                >> $ENV_FILE
    echo "ACCESS_TOKEN_SECRET=${ACCESS_TOKEN_SECRET}"  >> $ENV_FILE
} &> /dev/null