# Back-end

Repositório com código fonte do back-end da aplicação.

**Importante**

Todos os comandos aqui apresentados devem ser executados na raiz deste projeto.

## Pré-requisitos

Antes da execução da aplicação é preciso verificar se estão instalados todos os recursos necessários para a execução do front-end. Abaixo segue a lista com os recursos.

- Plataforma NodeJS
- Gerenciador de pacotes NPM
- Banco de dados CouchDB
- Plataforma de container Docker

## Instalação

Primeiro faço *download* de todas as dependências do projeto.

```bash
npm i
```

## Execução

Utilize o comando abaixo para executar a aplicação.

```bash
npm run dev
```

Obs.: Garanta que não há nenhum processo sendo executado na porta 3000.

## Testes

Execute os testes da aplicação com o comando abaixo:

```bash
npm test
```

## Geração imagem Docker

Caso seja necessário gerar a imagem Docker da aplicação execute o seguinte comando.

```bash
docker build -t backend_casetwitter .
```

## Execução em docker-compose

Para executar todo o projeto em `docker-compose` acesse [este projeto]()