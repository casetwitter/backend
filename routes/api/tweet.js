const router = require('express').Router();
const alasql = require('alasql');
const util = require('../../util/Util');
const databaseService = require('../../service/DatabaseService');
const twitterService = require('../../service/TwitterService');

/**
 * @desc Busca tweets por uma determinada hashtag. Se hashtag passada estiver na lista `hashtags` os tweets são salvos no banco de dados.
 * @parm string $hashtag - hashtag a ser pesquisada
 * @return JsonArray - lista com pelo menos 100 tweets da hashtag passada como parâmetro
 */
router.get('/', async (req, res) => {
    const hashtag = req.query.hashtag;
    let tweets = await twitterService.findTweetsByHashtag(hashtag);
    res.status(200).json(tweets);
    if (util.getHashtags().indexOf(hashtag) >= 0) {
        await runTweets(tweets);
    }
});

/**
 * @desc Busca no banco de dados quais são os Top 5 usuários com maiores seguidores por hashtag e geral (contando todas as hashtags)
 * @return JsonArray - lista com todos os Top 5 usuários
 */
router.get('/users', (req, res) => {
    databaseService.getDatabase().list({
        include_docs: true
    }).then((result) => {
        let listRanking = [];
        let rankingAll = alasql('SELECT DISTINCT a.name, a.followers, a.hashtag FROM ? AS a ORDER BY a.followers DESC LIMIT 5', [util.converterArray(result)]);
        for (const hashtag of util.getHashtags()) {
            let ranking = alasql('SELECT DISTINCT a.name, a.followers FROM ? AS a WHERE a.hashtag = ? ORDER BY a.followers DESC LIMIT 5', [util.converterArray(result), hashtag]);
            listRanking.push({
                hashtag,
                ranking
            });
        }
        listRanking.push({
            hashtag: 'Geral',
            ranking: rankingAll
        });
        res.status(200).json(listRanking);
    });
});

/**
 * @desc Busca todas as hashtags que estão armazenadas no banco de dados
 * @return JsonArray - lista com as hashtags armazenadas no banco de dados
 */
router.get('/hashtag/stored/', (req, res) => {
    databaseService.getDatabase().list({
        include_docs: true
    }).then((result) => {
        let list = alasql('SELECT DISTINCT a.hashtag FROM ? AS a', [util.converterArray(result)]);
        res.status(200).json(list);
    });
});

/**
 * @desc Percorre por todos os tweets da hashtag pesquisada anteriormente e chama o método `insertTweets` para inserir no banco de dados
 * @parm array $listTweets - lista com os tweets da hashtag pesquisada anteriormente
 * @return JsonArray - lista com as hashtags armazenadas no banco de dados
 */
async function runTweets(listTweets) {
    for (const item of listTweets) {
        databaseService.insertTweets(item);
    }
}

module.exports = router;