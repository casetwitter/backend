module.exports = {
    converterArray: (data) => {
        return data.rows.map((result) => result.doc)
    },
    getHashtags: () => {
        return ['#cloud', '#container', '#devops', '#aws', '#microservices', '#docker', '#openstack', '#automation', '#gcp', '#azure', '#istio', '#sre'];
    }
}