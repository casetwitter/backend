FROM node:10.15.3-alpine

WORKDIR /usr/app

COPY package.json .

RUN npm i --quiet

COPY . .

RUN npm i pm2 -g

CMD ["pm2-runtime", "index.js"]